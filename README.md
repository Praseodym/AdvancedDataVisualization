# AdvancedDataVisualization

## US Flight Data

- Heat Map
- Geo Map

## Consumer Electronics Sales Data

- Dashboard (incl. Violin Chart)
- Line Chart
- Bar Charts and Line Chart combined
- Bar Chart and Tree Map combined
- Bar Chart horizontal
- Bar Chart horizontal
- WordCloud
- FaceGrid Charts
- Box Charts
- Swarm Chart
